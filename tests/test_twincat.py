import unittest
import struct
from pyads import constants

from tests.twincat_testcase import TwinCatTestCase, PLCVariable


class TwinCATTest(TwinCatTestCase):
    """Test the simulink classes."""

    def test_model_connection(self):
        """Test Simulink model when connected to TwinCAT.

        The module info and the specific symbol had been created in the test-case
        setup.
        """

        self.assertIsNone(self.model.MyConstant.Value._plc)

        self.model.connect_to_twincat(self.plc)

        self.assertIsNotNone(self.model.MyConstant.Value._plc)

        self.model.MyConstant.Value.write(15)
        value = self.model.MyConstant.Value.get()
        self.assertEqual(15, value)


class TwinCATSymbolsTest(TwinCatTestCase):
    """Test list features on connection class."""

    def setUp(self):

        super().setUp()

        self.symbols = [
            self.model.MyConstant.Value,
            self.model.ArrayConstant.Value,
            self.model.MySineWave.Amplitude,
        ]

    def test_read_list_of_symbol(self):
        """Test sum read operation for symbols."""

        result = self.plc.read_list_of_symbols(self.symbols)

        self.assertEqual(len(self.test_server.request_history), 1)
        # Make sure only a single ADS request was sent

        values = list(result.values())

        expected = [
            420.69,
            [0.654694, 0.99844],
            494949494.3
        ]

        self.assertEqual(expected, values)

        # Verify new cache
        self.assertEqual(expected,
                         [s._value for s in self.symbols])

    def test_write_list_of_symbol(self):
        """Test sum read operation for symbols."""

        new_values = [51.5, [65496.64, 97643.51], 0.019484]

        result = self.plc.write_list_of_symbols(dict(zip(self.symbols, new_values)))

        errors = list(result.values())

        self.assertEqual(["no error"] * 3, errors)

        # Verify new cache
        self.assertEqual(new_values,
                         [s._value for s in self.symbols])

        read_values = self.plc.read_list_of_symbols(self.symbols)

        self.assertEqual(new_values,
                         list(read_values.values()))


if __name__ == "__main__":
    unittest.main()
