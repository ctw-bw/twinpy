import sys
from PyQt5.QtWidgets import QApplication, QFormLayout, QLabel, QWidget
from PyQt5.QtGui import QDoubleValidator

from twinpy.ui import BaseGUI
from twinpy.ui import TcLabel, TcLineEdit
from twinpy.twincat import SimulinkModel
from twinpy.twincat import TwincatConnection


class TestGUI(BaseGUI):
    def __init__(self, controller: SimulinkModel, **kwargs):

        super().__init__(controller=controller, **kwargs)

        layout_label = QFormLayout()

        self.label_output = TcLabel(
            # symbol=controller.Sum.so1
        )

        self.input_box = TcLineEdit(
            # symbol=controller.MySineWave.Bias,
        )
        self.input_box.setValidator(QDoubleValidator())

        self.label_frequency = TcLabel(
            # symbol=controller.MySineWave.Frequency
        )

        layout_label.addRow(QLabel("Value:"), self.label_output)
        layout_label.addRow(QLabel("Bias:"), self.input_box)
        layout_label.addRow(QLabel("Frequency:"), self.label_frequency)

        widget_label = QWidget()
        widget_label.setLayout(layout_label)
        self.layout_left.addWidget(widget_label)


if __name__ == "__main__":

    tc = TwincatConnection()

    app = QApplication(sys.argv)

    model = SimulinkModel(0x01010010, 'Controller')
    # model = SimulinkModel(0x101020, "we2_actuator_model")

    model.connect_to_twincat(tc)

    gui = TestGUI(model)

    app.exec()
