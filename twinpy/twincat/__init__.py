"""TwinPy.TwinCAT module.

These imports allow the user to skip the file names in their import.
"""

from .connection import TwincatConnection
from .simulink import SimulinkModel, SimulinkBlock
from .symbols import Symbol, Parameter, Signal
