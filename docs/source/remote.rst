Using TwinPy with a remote target
=================================

You can also use TwinPy (and other pyads applications) remotely. In this scenario a client PC runs the GUI, interacting with a running TwinCAT instance on a target PC.

Using TwinPy remotely involves two steps:

 * Getting access to the compiled Simulink model's XML file for the model structure
 * Making a remote ADS connection

.. TIP::
   Often in a target and client situation, the client is also the development PC (which compiles the TwinCAT solution for the target).
   When this is the case, giving access to the XML files for TwinPy is trivial since they already exist on the client computer.


Access to the XML file
----------------------

When the client is not the computer that compiles the Simulink model, it won't automatically have access to the most
recent model XML. There are a few approaches:

 * Compile the model also on the client PC
     * This will require all the compile dependencies on the client PC, and each compilation has to be done twice.
 * Copy the XML from the compiled model to the client PC
     * No extra compilation is needed, but copying the XML each time can be time consuming.
 * Set up a file share to give direct access to the compiled XMLs
     * By sharing the XMLs directly a client has access to the model structure without extra steps


Sharing the XML files directly
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

On the development PC (could be the same as the target PC), create a network share for the TwinCAT modules directory.
By default this is ``C:\TwinCAT\3.1\CustomConfig\Modules``.
Simply right-click on the ``Modules`` directory in Windows explorer and click 'Give access to...'. Complete the wizard
by adding the client PC. Read-only permissions should be all that's needed.

On the client PC you should now be able to find the PC and view the modules. Note the absolute path, which might be
something like ``\\<ip>\Modules``.

Search for info on Windows file sharing in case you run into problems.


Remote ADS connection
---------------------

See the pyads documentation on routing for more information: https://pyads.readthedocs.io/en/latest/documentation/routing.html


Adding routes on Windows
~~~~~~~~~~~~~~~~~~~~~~~~

Use the TwinCAT UI and add the remote. Right-click on the TwinCAT icon in the taskbar and click 'Router...' > 'Edit Routes'.

This gif exemplifies the procedure:

.. figure:: ../images/add_route_to_target.gif

The route must *not* be set to 'unidirectional', which seems to be the default.

Note that you might need to add allow-rules in the firewall for both inbound and outbound traffic on TCP ports
48898 and 8016, and UPD port 48899.


Adding routes on Linux
~~~~~~~~~~~~~~~~~~~~~~

The :class:`pyads.connection.Connection` will create a route from the client to the target. You can then use
:func:`pyads.ads.add_route_to_plc` to create a route back to the client. Or you can use the TwinCAT UI on the remote target
to create the route back.


TwinPy Remote
-------------

In your application script, set the IP address, AMS net id and port correctly for the remote target when
instantiating :class:`~twinpy.twincat.connection.TwincatConnection`.

In case no local XML file is available, specify an absolute path to the XML file when creating a :class:`~twinpy.twincat.SimulinkModel`.
