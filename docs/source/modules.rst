Modules
=======



TwinCAT and Simulink Interfacing
--------------------------------


TwinCAT Connection
~~~~~~~~~~~~~~~~~~

.. automodule:: twinpy.twincat.connection


Simulink
~~~~~~~~

.. automodule:: twinpy.twincat.simulink
    :no-members:

.. autofunction:: twinpy.twincat.simulink.sanitize_name

SimulinkModel
"""""""""""""

.. autoclass:: twinpy.twincat.SimulinkModel

SimulinkBlock
"""""""""""""

.. autoclass:: twinpy.twincat.SimulinkBlock


ADS Variables
~~~~~~~~~~~~~

.. automodule:: twinpy.twincat.symbols
    :no-members:

Symbol
""""""

.. autoclass:: twinpy.twincat.Symbol

Parameter
"""""""""

.. autoclass:: twinpy.twincat.Parameter

Signal
""""""

.. autoclass:: twinpy.twincat.Signal



GUI
---


TwinCAT UI Elements
~~~~~~~~~~~~~~~~~~~

.. automodule:: twinpy.ui.tc_widgets
    :no-members:

TcWidget
""""""""

.. autoclass:: twinpy.ui.TcWidget

TcLabel
"""""""

.. autoclass:: twinpy.ui.TcLabel

TcLineEdit
""""""""""

.. autoclass:: twinpy.ui.TcLineEdit

TcPushButton
""""""""""""

.. autoclass:: twinpy.ui.TcPushButton

TcRadioButton
"""""""""""""

.. autoclass:: twinpy.ui.TcRadioButton

TcRadioButtonGroupBox
"""""""""""""""""""""

.. autoclass:: twinpy.ui.TcRadioButtonGroupBox

TcCheckBox
""""""""""

.. autoclass:: twinpy.ui.TcCheckBox

TcSlider
""""""""

.. autoclass:: twinpy.ui.TcSlider

TcGraph
"""""""

.. autoclass:: twinpy.ui.TcGraph

.. autoclass:: twinpy.ui.GraphWidget


Base GUI
~~~~~~~~

.. automodule:: twinpy.ui.base_gui
    :members:


Base Widgets
~~~~~~~~~~~~

.. automodule:: twinpy.ui.base_widgets
    :no-members:


TcErrorsLabel
"""""""""""""

.. autoclass:: twinpy.ui.TcErrorsLabel


DrivesWidget
""""""""""""

.. autoclass:: twinpy.ui.DrivesWidget


SystemBackpackWidget
""""""""""""""""""""

.. autoclass:: twinpy.ui.SystemBackpackWidget


SystemWRBSWidget
""""""""""""""""

.. autoclass:: twinpy.ui.SystemWRBSWidget


ErrorsWidget
""""""""""""

.. autoclass:: twinpy.ui.ErrorsWidget



Tabs
~~~~

.. automodule:: twinpy.ui.tabs
    :no-members:


ConsoleTab
""""""""""

.. autoclass:: twinpy.ui.ConsoleTab


FirmwareTab
"""""""""""

.. autoclass:: twinpy.ui.FirmwareTab



Main - Example
--------------

.. automodule:: twinpy.__main__



Module Info
-----------

.. automodule:: twinpy
    :no-members:
